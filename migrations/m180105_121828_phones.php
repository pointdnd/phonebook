<?php

use yii\db\Migration;

/**
 * Class m180105_121828_phones
 */
class m180105_121828_phones extends Migration
{

    public function up()
    {
        $this->createTable('phones', [
            'id' => $this->primaryKey(),
            'account_id' =>  $this->integer()->notNull(),
            'phone' =>  $this->string(12)->notNull(),


        ]);
    }

    public function down()
    {
        echo "m180105_121828_phones cannot be reverted.\n";
        $this->dropTable('phones');

    }

}
