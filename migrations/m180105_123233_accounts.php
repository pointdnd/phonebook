<?php

use yii\db\Migration;

/**
 * Class m180105_123233_accounts
 */
class m180105_123233_accounts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180105_123233_accounts cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('accounts', [
            'id' => $this->primaryKey(),
            'first_name' =>  $this->string(30)->notNull(),
            'second_name' =>  $this->string(30)->Null(),
            'last_name' =>  $this->string(30)->Null(),
        ]);

        $this->addForeignKey(
            'fk-accounts-account_id',
            'phones',
            'account_id',
            'accounts',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        echo "m180105_123233_accounts cannot be reverted.\n";
        $this->dropForeignKey(
            'fk-accounts-account_id',
            'phones'
        );
        $this->dropTable('accounts');
    }

}
