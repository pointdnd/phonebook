<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;




/* @var $this yii\web\View */
/* @var $model app\models\Accounts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-form">

    <?php $form = ActiveForm::begin([
            'id' => 'dynamic-form',
            'enableClientValidation'=>false,
    ]); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'second_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-phone-alt"></i> <?=$model->getAttributeLabel('phone')?></h4></div>

        <?php  DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 10, // the maximum times, an element can be cloned (default 999)
            'min' => 1, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $modelPhones[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'phone',
            ],
        ]); ?>

        <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($modelPhones as $i => $modelPhone): ?>
                <div class="item row">
                    <div class="col-xs-10">
                        <?php
                        if (! $modelPhone->isNewRecord) {
                            echo Html::activeHiddenInput($modelPhone, "[{$i}]id");
                        }
                        ?>
                        <?= $form->field($modelPhone, "[{$i}]phone")->widget(\yii\widgets\MaskedInput::className(),[
                            'mask' => '9 (999) 999-9999',
                            'clientOptions' => [

                                'removeMaskOnSubmit' => true,
                            ],
                            'options' => [
                                    'class'=>'form-control',
                                    'placeholder'=>'3 (123) 123-1230'
                            ]

                        ])->label(false) ?>
                    </div>
                    <div class="col-xs-2">
                        <button type="button" class="add-item btn btn-success "><i class="glyphicon glyphicon-plus"></i></button>
                        <button type="button" class="remove-item btn btn-danger "><i class="glyphicon glyphicon-minus"></i></button>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>

    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
