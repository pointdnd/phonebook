<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Accounts */

$this->title = $model->last_name.' '.$model->first_name.' '.$model->second_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Accounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accounts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'second_name',
            'last_name',
            ['attribute' => 'an_attributeid',
                'label' => 'Phones',
                'format'=>'raw',

                'value' => function($model) {
                    $html='';
                    foreach ($model->phones as $item) {
                        $formatedPhone=\app\models\Phones::convertPhone($item['phone']);
                        $html.=HTML::a($formatedPhone,'tel:'.$item['phone']).'<br>';
                    }
                    return $html;
                }
            ]
    ]
    ]) ?>

</div>
