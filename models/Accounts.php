<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%accounts}}".
 *
 * @property int $id
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%accounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name'], 'required'],
            [['first_name', 'second_name', 'last_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => Yii::t('app','First Name'),
            'second_name' => Yii::t('app','Second Name'),
            'last_name' => Yii::t('app','Last Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return AccountsQuery the active query used by this AR class.
     */

    public function getPhones()
    {
        return $this->hasMany(Phones::className(), ['account_id' => 'id']);
    }
}
