<?php

namespace app\models;

use Yii;
use uniqby\phoneFormatter\behaviors\PhoneFormatterBehavior;

/**
 * This is the model class for table "{{%phones}}".
 *
 * @property int $id
 * @property int $account_id link to Accounts.id
 * @property string $phone
 */
class Phones extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phones}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['account_id'], 'integer'],
            [['phone'], 'string','min'=>10, 'max'=>12],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account ID',
            'phone' => 'Phone',
        ];
    }

    /**
     * @param $number integer phone
     * @return null|string
     */

    public static function convertPhone($number)
    {
        return preg_replace('~.*(\d{1})(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '$1 ($2) $3-$4', $number);
    }



}
