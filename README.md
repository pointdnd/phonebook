Демонстрационный проект
Простая записная книжка
с возможностью добовления до 10 номеров телефонов

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition      
      config/             contains application configurations
      controllers/        contains Web controller classes      
      models/             contains model classes
      runtime/            contains files generated during runtime      
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



Требование
------------

PHP 5.4.0.
mysql



Установка
------------

~~~
git clone git@bitbucket.org:pointdnd/phonebook.git
cd phonebook
php composer.phar install

~~~

В случае если composer не установлен
-------------------------------------
 [Composer](http://getcomposer.org/)

Настройка базы данных
-------------

Правим файл `config/db.php` пример ниже:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=phonebook',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Создание таблиц

~~~
./yii migrate
~~~

Запуск

~~~
./yii serve
~~~

[Демо](http://localhost:8080/)

